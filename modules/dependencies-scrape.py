#!/usr/bin/env python3

import inspect
import os
import re
import sys


localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
import issuebot
from issuebot import IssuebotModule
from fdroidserver import common, metadata, scanner

# TODO should be replaced by fdroidserver.scanner.NON_FREE_GRADLE_LINES once its merged
NON_FREE_GRADLE_LINES = {
    exp: re.compile(r'.*' + exp, re.IGNORECASE) for exp in [
        r'flurryagent',
        r'paypal.*mpl',
        r'admob.*sdk.*android',
        r'google.*ad.*view',
        r'google.*admob',
        r'google.*play.*services',
        r'crittercism',
        r'heyzap',
        r'jpct.*ae',
        r'youtube.*android.*player.*api',
        r'bugsense',
        r'crashlytics',
        r'ouya.*sdk',
        r'libspen23',
        r'firebase',
        r'''["']com.facebook.android['":]''',
        r'cloudrail',
        r'com.tencent.bugly',
        r'appcenter-push',
    ]
}


class DependenciesScrape(IssuebotModule):
    def __init__(self):
        super().__init__()
        r = issuebot.requests_get('https://etip.exodus-privacy.eu.org/trackers/export')
        if r.status_code == 200:
            self.exodus_code_signatures = []
            for tracker in r.json()['trackers']:
                code_signature = tracker.get('code_signature')
                if code_signature:
                    self.exodus_code_signatures.append(re.compile(code_signature))

    def exodus_code_match(self, gradle_line):
        for pattern in self.exodus_code_signatures:
            if pattern.match(gradle_line):
                return True
        return False

    def non_free_code_match(self, gradle_line):
        for pattern in NON_FREE_GRADLE_LINES.values():
            if pattern.match(gradle_line):
                return True
        return False

    def main(self):
        build = metadata.Build()
        build.gradle = ['([a-z0-9]*)']  # use a pattern to match all flavors
        dependency_regexs = []
        for regex in scanner.get_gradle_compile_commands(build):
            dependency_regexs.append(
                re.compile(
                    r'^' + regex.pattern + r"""\s*\(?['"](\S+)['"]\)?.*""",
                    re.IGNORECASE,
                )
            )

        paths = common.get_all_gradle_and_manifests(self.source_dir)
        output = dict()
        trackers = []
        non_free = []
        for path in paths:
            if path.endswith('AndroidManifest.xml'):
                continue
            current_file = os.path.relpath(path, self.source_dir)
            if current_file not in output:
                output[current_file] = []
            with open(path) as fp:
                linenum = 0
                for line in fp.readlines():
                    linenum += 1
                    for regex in dependency_regexs:
                        for m in regex.finditer(line):
                            if m.lastindex == 1:
                                gradle_line = m.group(1)
                                flavor = ''
                            else:
                                gradle_line = m.group(2)
                                flavor = m.group(1)
                            output[current_file].append((gradle_line, linenum, flavor))
                            if self.non_free_code_match(gradle_line):
                                non_free.append((gradle_line, current_file, linenum, flavor))
                            if self.exodus_code_match(gradle_line):
                                trackers.append((gradle_line, current_file, linenum, flavor))
        if output:
            self.reply['reportData']['perFile'] = output
        self.reply['reportData']['problematicEntries'] = dict()
        if non_free:
            self.reply['reportData']['problematicEntries']['non-free'] = non_free
        if trackers:
            self.reply['reportData']['problematicEntries']['tracker'] = trackers

        report = ''
        for f, libraries in sorted(
            output.items(), key=lambda i: (len(i[0].split('/')), i[0])
        ):
            url = self.get_source_url(f)
            fileentry = ''
            for lib, linenum, flavor in sorted(libraries):
                fileentry += '''<tr><td><a href="{url}"><code>{lib}</code></a></td><td><tt>{flavor}</tt></td></tr>\n'''.format(
                    url=url + '#L' + str(linenum), lib=lib, flavor=flavor
                )
            if fileentry:
                report += '<details><summary><tt>%s</tt></summary><table>' % f
                report += (
                    '<tr><td><u>dependency</u></td><td><u>gradle flavor</u></td></tr>'
                )
                report += fileentry
                report += '</table></details>'
        if report:
            self.reply['report'] = (
                self.get_source_scanning_header(
                    'Dependencies scraped from gradle files'
                )
                + report
            )
        self.write_json()


if __name__ == "__main__":
    DependenciesScrape().main()
