#!/usr/bin/env python3

import inspect
import os
import sys

from fdroidserver import common, metadata, update

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..')
)
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
import issuebot
from issuebot import IssuebotModule


class Fastlane(IssuebotModule):
    def main(self):
        config = dict()
        common.fill_config_defaults(config)
        update.config = config
        for file in config['char_limits'].keys():
            config['char_limits'][file] += 1
        apps = {self.application_id: metadata.App()}
        update.copy_triple_t_store_metadata(apps)
        update.insert_localized_app_metadata(apps)
        if 'localized' in apps[self.application_id]:
            self.reply['labels'].add('fastlane')
            self.reply['reportData'][self.application_id] = apps[self.application_id]
        else:
            apps[self.application_id]['localized'] = dict()  # placeholder

        error = []
        report = ''
        localized = apps[self.application_id]['localized']
        if not localized:
            error.append('Please establish Fastlane in your repo!')
        if not localized.get('en-US') or not localized['en-US'].get('summary'):
            error.append('<code>en-US</code> is required!')
        for locale, v in localized.items():
            if not issuebot.BCP47_LOCALE_TAG_PATTERN.fullmatch(locale):
                locale_error = '<code>{locale}</code> is invalid!'.format(locale=locale)
                for replace in (('_', '-'), ('-r', '-'), ('_r', '-')):
                    if issuebot.BCP47_LOCALE_TAG_PATTERN.fullmatch(
                        locale.replace(*replace)
                    ):
                        locale_error += ' Do you mean {}?'.format(
                            locale.replace(*replace)
                        )
                        break
                error.append(locale_error)
            for file, limit in config['char_limits'].items():
                if v.get(file) and len(v[file]) == limit:
                    print(v[file])
                    error.append(
                        '<code>{file}</code> in <code>{locale}</code> should be shorter than {limit} characters!'.format(
                            file=file,
                            locale=locale,
                            limit=limit - 1,
                        )
                    )
            report += '<details>'
            report += '<summary>{locale}</summary>'.format(locale=locale)
            if v.get('name'):
                report += '<b>{}</b>'.format(v['name'])
            if v.get('summary') or v.get('description'):
                report += '<details><summary>{}</summary><br/>'.format(v.get('summary'))
            if v.get('description'):
                report += '<p>{}</p>'.format(v['description'])
            if v.get('summary') or v.get('description'):
                report += '</details>'
            report += '</details>'
        self.reply['report'] = '<h3>Fastlane</h3>'
        if error:
            self.reply['report'] += '<b>Error!</b><br>' + '<br>'.join(error) + '<br>'
        if report:
            self.reply['report'] += (
                '<details><summary>by locale</summary>' + report + '</details>'
            ).replace('\n', '<br>')
        self.write_json()


if __name__ == "__main__":
    Fastlane().main()
