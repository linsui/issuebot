#!/usr/bin/env python3

import issuebot
import os
import unittest


class IssuebotTest(unittest.TestCase):
    """test the core issuebot runner script"""

    def test_valid_appid_regex(self):
        should_be = [
            'foo.bar',
            'com.moez.QKSMS',
            'An.stop',
            'SpeedoMeterApp.main',
            'a2dp.Vol',
            'click.dummer.UartSmartwatch',
            'com.Bisha.TI89EmuDonation',
            'org.f_droid.fdr0ID',
        ]
        found = []
        for appid in issuebot.APPLICATION_ID_PATTERN.findall(
            """
            ApplicationID:
               foo.bar
            Application ID: com.moez.QKSMS
            package name = "An.stop",
            package ID:		SpeedoMeterApp.main
            packageName:a2dp.Vol
            packageName: 'click.dummer.UartSmartwatch'
            Package = 'com.Bisha.TI89EmuDonation'
            appid = 'org.f_droid.fdr0ID'
        """
        ):
            found.append(appid)
        self.assertEqual(should_be, found)

    def test_invalid_appid_regex(self):
        valid = []
        for appid in issuebot.APPLICATION_ID_PATTERN.findall(
            """
            ID: foo.bar
            ApplicationID: 0.com.moez.QKSMS
            package name = "_An.stop",
            pID: "SpeedoMeterApp.main",
            pkgName:a2dp.Vol
            pkg 'click.dummer.UartSmartwatch'
            Name = 'com.Bisha.TI89EmuDonation'
            apid = 'org.f_droid.fdr0ID'
        """
        ):
            print(appid)
            valid.append(appid)
        self.assertEqual(0, len(valid))


if __name__ == "__main__":
    os.chdir(os.path.dirname(__file__))
    newSuite = unittest.TestSuite()
    newSuite.addTest(unittest.makeSuite(IssuebotTest))
    unittest.main(failfast=False)
